import openai
import json
import os
import requests
import uuid
import json
from flask import Flask, request, jsonify, send_file, render_template


config = json.load(open("config.json"))

# Add your OpenAI API key
OPENAI_API_KEY = config["openai_key"]
openai.api_key = OPENAI_API_KEY

# Add your ElevenLabs API key
ELEVENLABS_API_KEY = config["elevenlabs_key"]
ELEVENLABS_VOICE_STABILITY = 0.30
ELEVENLABS_VOICE_SIMILARITY = 0.75

# Choose your favorite ElevenLabs voice
ELEVENLABS_VOICE_NAME = "male_voice_one"
ELEVENLABS_ALL_VOICES = []

app = Flask(__name__)


def retrieve_voices() -> list:
    """Retrieve all voices from ElevenLabs API

    Returns
    -------
    list
        List of all voices
    """
    url = "https://api.elevenlabs.io/v1/voices"
    headers = {"xi-api-key": ELEVENLABS_API_KEY}
    response = requests.get(url, headers=headers)
    return response.json()["voices"]


def transcribe_audio(filename: str) -> str:
    """Transcribe audio file using OpenAI's Whisper API

    Parameters
    ----------
    filename : str
        FIlename of the audio file to be transscribed

    Returns
    -------
    str
        Transciption of the audio file
    """
    with open(filename, "rb") as audio_file:
        transcript = openai.Audio.transcribe("whisper-1", audio_file)
    return transcript.text


def generate_reply(conversation: list) -> str:
    """Generate a reply using Rasa's REST API

    Parameters
    ----------
    conversation : list
        The conversation history

    Returns
    -------
    str
        The generated reply from the Rasa chatbot
    """
    text = conversation[-1]["content"]
    payload = json.dumps({"sender": "Rasa", "message": text})
    headers = {"Content-type": "application/json", "Accept": "text/plain"}
    response = requests.request(
        "POST",
        url="http://localhost:5005/webhooks/rest/webhook",
        headers=headers,
        data=payload,
    )
    response = response.json()
    if len(response) > 1:
        cache = " ".join(statement["text"] for statement in response)
    else:
        cache = response[0]["text"]
        if "buttons" in response[0]:
            buttons = "\n".join(button["title"] for button in response[0]["buttons"])
            cache += f" {buttons}"
    return cache


def generate_audio(text: str, output_path: str = "") -> str:
    """Generate audio file using ElevenLabs' REST API

    Parameters
    ----------
    text : str
        The RASA chatbot's reply
    output_path : str, optional
        Output path of the generated audio file, by default ""

    Returns
    -------
    str
        Output path of the generated audio file
    """
    voices = ELEVENLABS_ALL_VOICES
    try:
        voice_id = next(filter(lambda v: v["name"] == ELEVENLABS_VOICE_NAME, voices))[
            "voice_id"
        ]
    except StopIteration:
        voice_id = voices[0]["voice_id"]
    url = f"https://api.elevenlabs.io/v1/text-to-speech/{voice_id}"
    headers = {"xi-api-key": ELEVENLABS_API_KEY, "content-type": "application/json"}
    data = {
        "text": text,
        "voice_settings": {
            "stability": ELEVENLABS_VOICE_STABILITY,
            "similarity_boost": ELEVENLABS_VOICE_SIMILARITY,
        },
    }
    response = requests.post(url, json=data, headers=headers)
    with open(output_path, "wb") as output:
        output.write(response.content)
    return output_path


@app.route("/")
def index():
    return render_template("index.html", voice=ELEVENLABS_VOICE_NAME)


@app.route("/transcribe", methods=["POST"])
def transcribe():
    """Transcribe audio file using OpenAI's Whisper API

    Returns
    -------
    dict
        Transcription of the audio file
    """
    if "file" not in request.files:
        return "No file found", 400
    file = request.files["file"]
    recording_file = f"{uuid.uuid4()}.wav"
    recording_path = f"uploads/{recording_file}"
    os.makedirs(os.path.dirname(recording_path), exist_ok=True)
    file.save(recording_path)
    transcription = transcribe_audio(recording_path)
    return jsonify({"text": transcription})


@app.route("/ask", methods=["POST"])
def ask():
    """Generate a Rasa response from the given conversation, then convert it to audio using ElevenLabs."""
    conversation = request.get_json(force=True).get("conversation", "")
    reply = generate_reply(conversation)
    reply_file = f"{uuid.uuid4()}.mp3"
    reply_path = f"outputs/{reply_file}"
    os.makedirs(os.path.dirname(reply_path), exist_ok=True)
    generate_audio(reply, output_path=reply_path)
    return jsonify({"text": reply, "audio": f"/listen/{reply_file}"})


@app.route("/listen/<filename>")
def listen(filename):
    """Return the audio file located at the given filename."""
    return send_file(f"outputs/{filename}", mimetype="audio/mp3", as_attachment=False)


if ELEVENLABS_API_KEY:
    if not ELEVENLABS_ALL_VOICES:
        ELEVENLABS_ALL_VOICES = retrieve_voices()
    if not ELEVENLABS_VOICE_NAME:
        ELEVENLABS_VOICE_NAME = ELEVENLABS_ALL_VOICES[0]["name"]

if __name__ == "__main__":
    app.run()
