<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<a name="readme-top"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/FrancisTembo/libbot_voice">
    <img src="static/photo.webp" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Libbot voice generator API</h3>

</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installing-requirements">Installing Requirements</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project
This subproject is part of NWU's library chatbot and utilizes OpenAI's Whisper API for audio transcription. The transcribed text is then passed as input to the RASA chatbot engine. The chatbot's response is converted into an audio response using the Elevenlabs API. 

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Installing Requirements

1. Install python:
    ```sh
    sudo apt update
    sudo apt install python3.8
    ```  
2. Install poetry:
   ```sh
   curl -sSL https://install.python-poetry.org | python3 -
   ```
3. Clone the repo - make sure you have the correct access:
   ```sh
   git clone https://gitlab.com/FrancisTembo/libbot_voice
   cd libbot_voice
   ```
4. Install dependancies:
   ```sh
   poetry install
   ```
5. Activate poetry environment
   ```sh
   poetry shell
   ```

<!-- USAGE EXAMPLES -->
## Usage

1. Start the Rasa assistant found at: https://gitlab.com/FrancisTembo/libbot
2. Create a file called config.json and enter your OpenAI and ElevenLabs API keys:
   ```json
    {
      "openai_key" : "YOUR_API_KEY",
      "elevenlabs_key" : "YOUR_API_KEY"
    }
   ```
3. Starting the bot in the terminal
   ```python
   python3 app.py
   ```
<!-- LICENSE -->
## License

This repository belongs to the North-West University LIbrary

<!-- CONTACT -->
## Contact

Francis Tembo: francistembo92@gmail.com